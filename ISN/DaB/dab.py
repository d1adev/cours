user_1 = 'Trucmuche'
user_2 = 'Bidule'
code_1 = '1234'
code_2 = '4321'
compte_1 = 500
compte_2 = 300

while True:#boucle infinie
    essai = input('Code?')
    i = 1
    while essai != code_1 and essai != code_2 and i <= 3:
        print('Mauvais code. Reste ' + str((3 - i)) + ' essais.')
        essai = input()
        i += 1

    if i <= 3:
        if essai == code_1:
            print('Bonjour ' + user_1)
            montant = input('Montant?')
            if int(montant) > 0 and int(montant) < 700:
                if (compte_1 - int(montant)) > 0:
                    compte_1 -= int(montant)
                    print('Ok, il vous reste ' + str(compte_1))
                else:
                    print('Pas assez de money')
            else:
                print('Opération annulée')
            print('OK.')
        elif essai == code_2:
            print('Bonjour ' + user_2)
            montant = input('Montant?')
            if int(montant) > 0 and int(montant) < 700:
                if (compte_1 - int(montant)) > 0:
                    compte_2 -= int(montant)
                    print('Ok, il vous reste ' + str(compte_2))
                else:
                    print('Pas assez de money')
            else:
                print('Opération annulée')
            print('OK.')

    else:
        print('Opération annulée')
    print('reset')
    i = 0
    essai = ""
    montant = 0

