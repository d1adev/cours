#coding=utf-8

from random import randint


class Pendu:
	def __init__(self):
		self._wordList = self._getWords()
		self._word = self._getRandomWord()
		self._tries = []
		self._won = False
		self.Start()
		
	def _getWords(self):
		f = open('mots.txt')
		retVal =  []
		lines = f.readlines()
		f.close()
		for w in lines:
			if len(w) > 5: 
				retVal.append(w.replace('\n', ''))
		return retVal
		
	def _parse(self): # Pas bien. TODO : trouver une version bien implemente dans un dll 
		self._word = self._word.replace(u'é', 'e')
		self._word = self._word.replace(u'è', 'e')
		self._word = self._word.replace(u'à', 'a')
		
		
	def _getRandomWord(self):
		return self._wordList[randint(0, len(self._wordList))].lower()
		
	def _update(self):
		rpz = ''
		for c in self._word:
			if c in self._tries:
				rpz += ' ' + c + ' '
			else:
				rpz += ' _ '
		if rpz.replace(' ', '') == self._word:
			self._won = True
		return [' '.join(self._tries), rpz]
		
		
	def Start(self):
		print(str(len(self._wordList)) + ' mots disponibles')
		print('Mot choisi contient ' + str(len(self._word)) + ' lettres.')
		while not self._won:
			u = self._update()
			if self._won:
				print('Gagne!')
				print('Vos essais : ' + u[0] + '\n' + u[1])
			else:
				print('Vos essais : ' + u[0] + '\n' + u[1])
				i = raw_input('Entrez une lettre : ') #changer pour input() sur python 3
				self._tries.append(i)
		
#p = Pendu()

def parse():
	f = open('mots.txt')
	lines = f.readlines()
	f.close()
	letters = 'abcdefghijklmnopqrstuvwxyz0'
	n = []
	nbr = [0]*27
	for c in letters:
		n.append(c)
	for l in lines:
		for c in l.lower().replace('\n', ''):
			try:
				nbr[n.index(c)] += 1
			except Exception:
				nbr[26] += 1
	for i in range(0, len(nbr)):
		print(n[i] + ' - ' + str(nbr[i]))

parse()



	