#######################################
# La fonction ligne_vierge()
#######################################
# Argument(s)  : n un entier

# Retourne : la liste ['', [ '','',''],['','',''],.......,['','',''],['','','']] avec n-1 elements du type ['','','']
#######################################
def ligne_vierge(n):
    ligne=['']
    for i in range(n-1):
        ligne=ligne+[['','','']]
    return ligne
#######################################

#######################################
# La fonction extract_derniere_ligne()
#######################################
# Argument(s) : - une liste definie au prealable

# Retourne : le dernier element de cette liste (qui peut etre lui meme une liste)
#######################################
def extract_derniere_ligne(liste):
    return liste[len(liste)-1]
#######################################

#######################################
# La fonction recherche_derniere_selection()
#######################################
# Argument une liste dont les elements d'index superieurs a  1 sont des sous-listes a  3 elements.

# retourne l'index de la sous-liste a  3 elements qui contient 'd' en 2eme element.
def recherche_derniere_selection(liste):
    i=1
    for i in range(1,len(liste)):
        if liste[i][2]=='d':
            return  i
#######################################
# La fonction construire_nouveau_tableau()
#######################################
# Argument(s)  : 	- une liste1
# 			- une liste2

# Retourne  :  la liste1 augmentee d'un element supplementaire qui est liste2  .

#######################################
def construire_nouveau_tableau(liste1,liste2):
    return liste1+[liste2]
#######################################


#######################################
# La fonction chemin_termine()
#######################################
# Argument(s) : - une liste structuree comme les listes etape_i decrites avant.

# Retourne : True si la liste  etape_i utilisee en argument indique que la ville A est atteinte et False sinon.
#######################################
def ville_arrive_atteinte(liste):
        print('argument de ville_arrive_atteinte  :  ',liste)
        if liste[len(liste)-1][2]=='d':
            return True
        else:
            return False


#######################################
# programme principal
#######################################
# importation des fonctions pre-programmees de ce TP2
# il s'agit de :
# TP2.nouvelle_etape()
# et de
# TP2.pour_affichage_chemin_plus_court()
# utilisees dans ce programme mais dont les scripts sont dans le fichier TP2.py
import TP2
# Creation d'un exemple de graphe des distances avec lequel nous rechercherons le chemin le plus court : 

# Les distances a partir de la ville de depart 
L0 = [0,3,1,-1,-1,-1]
# Les distances a partir de chaque ville intermediaire
L1 = [3,0,1,2,-1,-1]
L2 = [1,1,0,3,5,-1]
L3 = [-1,2,3,0,1,3]
L4 = [-1,-1,5,1,0,1]
# Les distances a partir de la ville d'arrivee
L5 = [-1,-1,-1,3,1,0]
# La matrice de toutes les distances : 
graphe = [L0,L1,L2,L3,L4,L5]
# et la taille de cette matrice
taille_graphe=len(graphe)

# initialisation du tableau de recherche :
tableau_recherche=[]

# Creation d'une ligne vierge pour le tableau de recherche :
nouvelle_ligne_vierge=ligne_vierge(taille_graphe)
print('Ligne vierge : ' + str(nouvelle_ligne_vierge))

# Construction de la premiere ligne du tableau de recherche a partir de nouvelle_ligne_vierge et du graphe des distances : 
tableau_recherche=TP2.construire_premiere_ligne(nouvelle_ligne_vierge,graphe)
print('tab rech : ' + str(tableau_recherche))

# Construction des autres lignes tant que la ville d'arrivee n'est pas atteinte : 
while(not ville_arrive_atteinte(extract_derniere_ligne(tableau_recherche))):
    print('tab' + str(tableau_recherche))
    # extraction de la derniere ligne du tableau de recherche :
    derniere_ligne=extract_derniere_ligne(tableau_recherche)
    # recherche de l'index du dernier element selectionne dans cette derniere ligne :
    index_derniere_selection=recherche_derniere_selection(derniere_ligne)	
    # construction d'une nouvelle ligne vierge  :
    nouvelle_ligne_vierge=ligne_vierge(taille_graphe)
    # construction d'une nouvelle ligne du tableau de recherche grace a derniere_ligne  :
    nouvelle_ligne=TP2.nouvelle_ligne(index_derniere_selection,derniere_ligne,nouvelle_ligne_vierge,graphe)
    # construction du nouveau tableau a partir de l'ancien et de nouvelle_ligne
    tableau_recherche=construire_nouveau_tableau(tableau_recherche,nouvelle_ligne)
   
# preparation de l'affichage : 
tableau_pour_affichage=TP2.pour_affichage_chemin_plus_court(tableau_recherche)
# affichage
print(tableau_pour_affichage)

#######################################

