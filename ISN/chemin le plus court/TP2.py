﻿
def construire_premiere_ligne(ligne_vierge,graphe):
	min,index_min=10**20,0
	for i in range(1,len(ligne_vierge)):
		ligne_vierge[i][0]=graphe[i][0]
		if 0<=ligne_vierge[i][0]<min :
			min=ligne_vierge[i][0]
			index_min=i
		ligne_vierge[i][1]=0
		ligne_vierge[i][2]='n'
	ligne_vierge[index_min][2]='d'
	return [ligne_vierge]




# La fonction nouvelle_etape()  :
# Argument(s)  :
#           - un entier qui est l'index de la derniere ville selectionnee
#           - une liste structuree comme etape_i  decrite avant mais ou la ville selectionnee n'est
#                                     pas encore la ville d'arrivee.
#			- une liste de type retourne par la fonction etape_vierge()
#			- une liste (matrice) du type "graphe des distances"  telle qu'elle est decrite en debut de phase 2

# Retourne  :		- une liste du structuree comme les listes etape_i qui constitue une etape de plus dans la
# 			   recherche du chemin le plus court.
# - un message d'erreur lorsque la construction n'est pas possible.

def nouvelle_ligne(index,liste1,liste_vierge,matrice):
    nouvelle_etape=liste_vierge
    index_selection=index
    distance_parcourue_via_der_selection=liste1[index_selection][0]
    # print('index_selection  :   ',index_selection)
    # print('distance_parcourue_via_der_selection  :   ',distance_parcourue_via_der_selection)

    try:

        for i in range(1,len(liste1)):
            if i==index_selection:
                # Si l'element etait la derniere selection  , il devient inamovible
                # en rentrant dans la categorie des 'o' donc non reutilisable
                nouvelle_etape[i][0]=liste1[i][0]
                nouvelle_etape[i][1]=liste1[i][1]
                nouvelle_etape[i][2]='o'
            elif liste1[i][2]=='o' or liste1[i][2]=='d':
                # si l'element a deja  ete selectionne auparavant(critere 'o') , on ne le modifie pas !
                nouvelle_etape[i]=liste1[i]
            else :
                # sinon , on actualise la distance a  parcourir jusqu'a  lui via la derniere ville selectionnee.
                # lorsque celle-ci est strictement inferieure a  celle deja  evaluee pour cette ville
                if matrice[index_selection][i]!=-1 and (distance_parcourue_via_der_selection+matrice[index_selection][i]<liste1[i][0] or liste1[i][0]==-1) :
                    # dans le cas ou la ville est atteignable a  partir de la precedente etape , on cumule avec la distance_parcourue
                    nouvelle_etape[i][0]=distance_parcourue_via_der_selection+matrice[index_selection][i]
                    # la ville de provenance devient la derniere selection
                    nouvelle_etape[i][1]=index_selection
                    nouvelle_etape[i][2]='n'
                else :
                    # sinon la ville garde ses proprietes
                    nouvelle_etape[i]=liste1[i]



            # il reste a  marquer la nouvelle ville selectionnee par appel de la fonction  marque_nouvelle_selection()
        nouvelle_etape=marque_nouvelle_selection(nouvelle_etape)
        return nouvelle_etape
    except:
        print("Desole , la construction n'a pas pu etre effectuee !")
        print(liste1)
        return None
#######################################
# La fonction pour_affichage_chemin_plus_court()
#######################################
# Argument(s)  : - une liste structuree comme liste_etapes  decrite avant et permettant la lecture du chemin le
#                           plus court  depuis la ville de depart jusqu'a  la ville d'arrivee.

# Retourne  :  	- une liste qui donne les numeros de villes utilisees et la distance deja  parcourue jusqu'a  cette ville
#######################################
def pour_affichage_chemin_plus_court(liste):
	try : 
		# la derniere etape est la ville A
		affichage=[['ville numero : ',recherche_derniere_selection(extract_derniere_etape(liste)),'distance depuis le depart : ',extract_derniere_etape(liste)[recherche_derniere_selection(extract_derniere_etape(liste))][0]]]
		# la distance totale pour l'atteindre est affichee :
		provenance=extract_derniere_etape(liste)[recherche_derniere_selection(extract_derniere_etape(liste))][1]
		i=len(liste)-1
		while (i>=1):
			i-=1
			if liste[i][provenance][2]=='d':
				affichage=['ville numero : ',provenance,'distance depuis le depart : ',liste[i][provenance][0]]+affichage
				provenance=liste[i][provenance][1]

		return ['ville numero : ',0,'distance depuis le depart : ',0]+affichage
	except : 
		print("Desole , la construction n'a pas pu etre effectuee !")
		return None
#######################################
# La fonction extract_derniere_etape()
#######################################
# Argument(s)  : - une liste definie au prealable

# Retourne  : le dernier element de cette liste (qui peut etre lui meme une liste)
#######################################
def extract_derniere_etape(liste):
    return liste[len(liste)-1]
#######################################



###############################################################################################
# Argument une liste structuree comme "etape_i"

# retourne l'index de la ville qui a ete la derniere selectionnee dans la liste fournie
def recherche_derniere_selection(liste):
    i=1
    for i in range(1,len(liste)):
        if liste[i][2]=='d':
            return  i
    # remarque on peut raccourcir et ne pas utiliser de compteur de boucle i en utilisant la methode liste.index(element[2]

###############################################################################################
# Argument : une liste structuree comme etape_i ou aucune ville n'est encore de categorie 'd'

# Retourne : la meme liste qu'en argument mais contenant la derniere selection (ville marquee 'd')
def marque_nouvelle_selection(liste):
    #reperage de l'index de la ville possedant la distance minimale chez les villes de critere 'n'
    index=0
    distance_mini=10**20
    for i in range(1,len(liste)):
        if liste[i][0]<distance_mini and liste[i][2]=='n' and liste[i][0]!=-1 :
            distance_mini=liste[i][0]
            index=i
    # marquage par 'd' de la ville selectionnee
    liste[index][2]='d'
    return liste
################################################################################################


